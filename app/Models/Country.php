<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 *
 * @property int $id
 * @property string $capital
 * @property string $citizenship
 * @property string $country_code
 * @property string $currency
 * @property string $currency_code
 * @property string $currency_sub_unit
 * @property string $currency_symbol
 * @property int $currency_decimals
 * @property string $full_name
 * @property string $iso_3166_2
 * @property string $iso_3166_3
 * @property string $name
 * @property string $region_code
 * @property string $sub_region_code
 * @property int $eea
 * @property string $calling_code
 * @property string $flag
 *
 * @package App\Models
 */
class Country extends BaseModel
{
    protected $table = 'countries';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
