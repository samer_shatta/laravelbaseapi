<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 6/14/17
 * Time: 4:28 AM
 */

namespace App\Library\Transformers;


use App\Http\Enums\SocialPlatform;
use App\Http\Helpers;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends BaseTransformerAbstract{
    const IMAGES_PATH = "images/uploads/users/";
    private $is_minified = false;
    private $default_user_image = 'default-user.jpg';

    protected $defaultIncludes = [

    ];

    public function __construct($isMinified = false,$withCountry = true){
        $this->isMinified = $isMinified;

        if($withCountry) $this->defaultIncludes = ['country'];
    }

    public function transform(User $item){
        $returned_date = [];
        if($this->isMinified){
            $returned_date = $this->beatify([
                'id' => (string)$item->id,
                'email' => $item->email,
                'name' =>$item->name,
                'gender' => $item->gender,
                'phone' => $item->phone,
                'photo' => $item->photo == '' ? Helpers::getImageFullPath($this->default_user_image,self::IMAGES_PATH) : Helpers::getImageFullPath($item->photo,self::IMAGES_PATH),
                'isActive' => $item->is_active == 1 ? true : false,
                'isVerified' => $item->is_verified == 1 ? true : false,
            ]);
        }else{
            $returned_date = $this->beatify([
                'id' => (string)$item->id,
                'email' => $item->email,
                'name' =>$item->name,
                'gender' => $item->gender,
                'phone' => $item->phone,
                'address' => $item->address,
                'birthday' => $item->birthday,
                'photo' => $item->photo == '' ? Helpers::getImageFullPath($this->default_user_image,self::IMAGES_PATH) : Helpers::getImageFullPath($item->photo,self::IMAGES_PATH),
                'token' => $item->token,
                'isActive' => $item->is_active == 1 ? true : false,
                'isVerified' => $item->is_verified == 1 ? true : false,
            ]);
        }
        if($item->social_id != '' && $item->social_platform == SocialPlatform::FACEBOOK)
            $returned_date['photo'] = "https://graph.facebook.com/{$item->social_id}/picture?type=normal";

        return $returned_date;
    }

    public function includeCountry(User $item){
        return $this->item($item->country,new CountryTransformer(false));
    }
}