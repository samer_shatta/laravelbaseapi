<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Helpers;
use App\Library\Transformers\CategoryTransformer;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mockery\CountValidator\Exception;

class CategoriesController extends ApiController
{
    /**
     * @api {get} /categories Categories List
     * @apiName Categories List
     * @apiGroup Categories
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {"data":[{"id":"1","nameEn":"cat1","nameAr":"\u0635\u0646\u0641\u0661","cover":"","icon":""},{"id":"2","nameEn":"cat2","nameAr":"\u0635\u0646\u0641\u0662","cover":"","icon":""}]}
     *
     * @apiError {String} UNKNOWN_EXCEPTION Unknown Exception.
     *
     * @apiErrorExample {json} Error-Response:
     * {"error":{"code":"UNKNOWN_EXCEPTION","message":" in \/Applications\/MAMP\/htdocs\/tapdrive\/api\/app\/Http\/Controllers\/Api\/v1\/UsersController.php in Line :127","details":[]}}
     */
    public function index()
    {
        try {
            $categories = Category::all();
            return $this->respond([
                'data' => Helpers::transformArray($categories, new CategoryTransformer())
            ]);
        } catch (Exception $ex) {
            return $this->respondUnknownException($ex);
        }
    }

    /**
     * @api {post} /categories Store Category
     * @apiName Store Category
     * @apiGroup Categories
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {"data":{"data":{"id":"4","nameEn":"toto","nameAr":"\u062a\u0648\u062a\u0648","cover":"","icon":""}},"message":"Item created successfully"}
     *
     * @apiError {String} VALIDATION_ERROR validation failed
     * @apiError {String} UNKNOWN_EXCEPTION Unknown Exception.
     *
     * @apiErrorExample {json} Error-Response:
     * {"error":{"code":"UNKNOWN_EXCEPTION","message":" in \/Applications\/MAMP\/htdocs\/tapdrive\/api\/app\/Http\/Controllers\/Api\/v1\/UsersController.php in Line :127","details":[]}}
     */
    public function store(CreateCategoryRequest $request)
    {
        try{
            $category_attributes = [
                'name_en' => $request->input('nameEn',''),
                'name_ar' => $request->input('nameAr',''),
            ];

            $category = Category::create($category_attributes);
            return $this->respondCreated(Helpers::transformObject($category, new CategoryTransformer()));
        }catch (\Exception $ex){
            return $this->respondUnknownException($ex);
        }
    }

    /**
     * @api {get} /categories/{id} Get Category
     * @apiName Get Category
     * @apiGroup Categories
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {"data":{"id":"2","nameEn":"cat2","nameAr":"\u0635\u0646\u0641\u0662","cover":"","icon":""}}
     *
     * @apiError {String} MODEL_NOT_FOUND MODEL NOT FOUND.
     *
     * @apiErrorExample {json} Error-Response:
     * {"error":{"code":"MODEL_NOT_FOUND","message":"","details":[]}}
     *
     * @apiError {String} UNKNOWN_EXCEPTION Unknown Exception.
     *
     * @apiErrorExample {json} Error-Response:
     * {"error":{"code":"UNKNOWN_EXCEPTION","message":" in \/Applications\/MAMP\/htdocs\/tapdrive\/api\/app\/Http\/Controllers\/Api\/v1\/UsersController.php in Line :127","details":[]}}
     */
    public function show($id)
    {
        try{
            $category = Category::find($id);
            if($category == null)
                return $this->respondModelNotFound();
            return $this->respond(['data' => Helpers::transformObject($category,new CategoryTransformer())]);
        }catch (\Exception $ex){
            return $this->respondUnknownException($ex);
        }
    }

    /**
     * @api {put} /categories Update Category
     * @apiName Update Category
     * @apiGroup Categories
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {"data":{"data":{"id":"2","nameEn":"koko","nameAr":"\u0643\u0648\u0643\u0648","cover":"","icon":""}},"message":"Item updated successfully"}
     *
     * @apiError {String} MODEL_NOT_FOUND MODEL NOT FOUND.
     * @apiError {String} VALIDATION_ERROR validation failed
     * @apiError {String} UNKNOWN_EXCEPTION Unknown Exception.
     *
     * @apiErrorExample {json} Error-Response:
     * {"error":{"code":"UNKNOWN_EXCEPTION","message":" in \/Applications\/MAMP\/htdocs\/tapdrive\/api\/app\/Http\/Controllers\/Api\/v1\/UsersController.php in Line :127","details":[]}}
     */
    public function update(CreateCategoryRequest $request, $id)
    {
        try{
            $category = Category::find($id);
            if($category == null)
                return $this->respondModelNotFound();
            $category->name_en = $request->input('nameEn','');
            $category->name_ar = $request->input('nameAr','');
            $category->save();
            return $this->respondUpdated(Helpers::transformObject($category,new CategoryTransformer()));
        }catch (\Exception $ex){
            return $this->respondUnknownException($ex);
        }
    }

    /**
     * @api {delete} /categories/{id} Delete Category
     * @apiName Delete Category
     * @apiGroup Categories
     *
     *
     * @apiSuccessExample {json} Success-Response:
     * {"message":"Item deleted successfully"}
     *
     * @apiError {String} MODEL_NOT_FOUND MODEL NOT FOUND.
     * @apiError {String} UNKNOWN_EXCEPTION Unknown Exception.
     *
     * @apiErrorExample {json} Error-Response:
     * {"error":{"code":"UNKNOWN_EXCEPTION","message":" in \/Applications\/MAMP\/htdocs\/tapdrive\/api\/app\/Http\/Controllers\/Api\/v1\/UsersController.php in Line :127","details":[]}}
     */
    public function destroy($id)
    {
        try{
            $user = Auth::user();
            $category = Category::find($id);
            if($category == null)
                return $this->respondModelNotFound();
            $category->delete();
            return $this->respondDeleted();
        }catch (\Exception $ex){
            return $this->respondUnknownException($ex);
        }
    }
}
